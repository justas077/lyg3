#include "rapidjson/document.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>

using namespace std;
using namespace thrust;

struct str {
  int deleteAmount;
  double length;
  char text[200];
};

struct sum_func {
  __device__ str operator()(str first, str second) {
    first.length += second.length;
    first.deleteAmount += second.deleteAmount;
    int index = 0;
    for (int i = 0; i < sizeof(first.text); i++) {
      if (first.text[i] == 0) {
        index = i;
        break;
      }
    }

    char *p = second.text;
    for (int j = 0; p[j]; j++)
      if (p[j] != ' ')
        first.text[index++] = p[j];

    return first;
  }
};

host_vector<str> read_file();
void write_to_file(host_vector<str> r);

int main(void) {
  host_vector<str> host_data = read_file();
  device_vector<str> device_data = host_data;
  str init;
  init.deleteAmount = 0;
  init.length = 0;
  strcpy(init.text, "");
  str res = reduce(device_data.begin(), device_data.end(), init, sum_func());
  host_vector<str> result_data;
  result_data.push_back(res);
  write_to_file(result_data);
}

host_vector<str> read_file() {
  host_vector<str> result(0);

  ifstream inFile;
  inFile.open("data.json");
  stringstream strStream;
  strStream << inFile.rdbuf();
  string fileString = strStream.str();

  rapidjson::Document document;
  const char *json = fileString.c_str();
  document.Parse(json);

  assert(document.IsArray());
  for (int i = 0; i < document.Size(); i++) {
    str s;
    for (rapidjson::Value::ConstMemberIterator itr = document[i].MemberBegin(); itr != document[i].MemberEnd(); ++itr) {
      string name = itr->name.GetString();
      if (name == "text") {
        string text = itr->value.GetString();
        char *begin = s.text;
        char *end = begin + sizeof(s.text);
        std::fill(begin, end, 0);
        strcpy(s.text, text.c_str());
      } else if (name == "length") {
        s.length = itr->value.GetDouble();
      } else if (name == "deleteAmount") {
        s.deleteAmount = itr->value.GetInt();
      }
    }
    result.push_back(s);
  }

  return result;
}

void write_to_file(host_vector<str> r) {
  ofstream rezFile;
  rezFile.open("rez.txt");
  rezFile << "                                                                                                                                                  text | deleteAmount |     length |\n------------------------------------------------------------------\n";
  for (int i = 0; i < r.size(); i++)
    rezFile << setw(100) << r[i].text << " | " << setw(12) << to_string(r[i].deleteAmount) << " | " << setw(10) << to_string(r[i].length) << " | " << endl;
  rezFile.close();
}
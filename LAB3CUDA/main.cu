#include <fstream>
#include <iostream>
#include <sstream>
#include "rapidjson/document.h"
#include <vector>
#include <cuda.h>
#include <stdio.h>
#include <iomanip>
#include <string>

using namespace std;

struct str {
  int deleteAmount;
  double length;
  char text[1000];
};

vector<str> read_file();
void write_to_file(str* r);
__global__ void sum(str* data, str* result, int size);


const int THREADS = 7;
int main(void) {
  vector<str> data = read_file();
  str dataArray[data.size()];
  str resultArray[THREADS];
  std::copy(data.begin(), data.end(), dataArray);
  
  str *device;
  str *res_device;
  int size = data.size() * sizeof(str);
  int res_size = THREADS * sizeof(str);
  
  cudaMalloc((void**)&device, size);
  cudaMalloc((void**)&res_device, size);
  cudaMemcpy(device, dataArray, size, cudaMemcpyHostToDevice);
  sum<<<1, THREADS>>>(device, res_device, data.size());
  cudaDeviceSynchronize();
  cudaMemcpy(resultArray, res_device, res_size, cudaMemcpyDeviceToHost);
  cudaFree(device);
  cudaFree(res_device);
  
  for(int i = 0; i < THREADS; i++) {
    printf("%s, %d, %f\n", resultArray[i].text, resultArray[i].deleteAmount, resultArray[i].length);
  }
  
  write_to_file(resultArray);
}

__global__ void sum(str* data, str* result, int size) {
  int s = size / THREADS;
  int l = size % THREADS;
  int th = threadIdx.x;
  
  str newStr;
  
  int start = th > l ? th * l - (th - l) : th * l;
  int end = start + s;
  if(th >= l)
    end -= 1;

  int totalDelete = 0;
  double totalLength = 0;
  int index = 0;
  for(int i = start; i <= end; i++) {
    totalDelete += data[i].deleteAmount;
    totalLength += data[i].length;
    char *p = data[i].text;
    for(int j = 0; p[j]; j++)
      if(p[j] != ' ')
        newStr.text[index++] = p[j];
  }
  
  newStr.deleteAmount = totalDelete;
  newStr.length = totalLength;
  result[th] = newStr;
}

vector<str> read_file() {
  vector<str> result(0);
  
  ifstream inFile;
  inFile.open("data.json");
  stringstream strStream;
  strStream << inFile.rdbuf();
  string fileString =  strStream.str();
  
  rapidjson::Document document;
  const char* json = fileString.c_str();
  document.Parse(json);
  
  assert(document.IsArray());
  for(int i = 0; i < document.Size(); i++) {
    str s;
    for (rapidjson::Value::ConstMemberIterator itr = document[i].MemberBegin(); itr != document[i].MemberEnd(); ++itr)
    {
      string name = itr->name.GetString();
      if(name == "text") {
        string text = itr->value.GetString();
        strcpy(s.text, text.c_str());
      } else if (name == "length") {
        s.length = itr->value.GetDouble();
      } else if (name == "deleteAmount") {
        s.deleteAmount = itr->value.GetInt();
      }
    }
    result.push_back(s);
  }
  
  return result;
}

void write_to_file(str* r) {
  ofstream rezFile;
  rezFile.open("rez.txt");
  rezFile << "                                    text | deleteAmount |     length |\n------------------------------------------------------------------\n";
  for (int i = 0; i < THREADS; i++)
    rezFile << setw(40) << r[i].text << " | " << setw(12) << to_string(r[i].deleteAmount) << " | " << setw(10) << to_string(r[i].length) << " | " << endl;
  rezFile.close();
}